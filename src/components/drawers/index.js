import React from 'react'
import { Drawer, ClickAwayListener , Paper} from '@material-ui/core';
import { AiFillCloseSquare } from 'react-icons/ai';
import { makeStyles } from '@material-ui/core/styles';
import './index.css';
import AOS from 'aos';
import 'aos/dist/aos.css';
AOS.init();

const useStyles = makeStyles({
    Paper: {
        backgroundColor: 'rgba(28,28,28,.90)',
    }
})


const NavDrawer = ({ isOpen, onHide }) => {
    const classes = useStyles();
    return (
        <ClickAwayListener onClickAway={onHide}>
            <Drawer
                variant='persistent'
                docked='false'
                anchor='top'
                open={isOpen}
                classes={{paper:classes.Paper}}>
                <div className="drawer-container" >
                    <div data-aos="fade-in" data-aos-duration="100" className="drawer-item">
                        <p className="nav-link">HOME</p>
                    </div>
                    <div data-aos="fade-in" data-aos-duration="1000" className="drawer-item">
                        <p className="nav-link">ABOUT</p>
                    </div>
                    <div data-aos="fade-in" data-aos-duration="2000" className="drawer-item">
                        <p className="nav-link">CONTACT US</p>
                    </div>
                    <div data-aos="fade-in" data-aos-duration="3000" className="drawer-item">
                        <p className="nav-link">NFPA CODES</p>
                    </div>
                </div>
                <div className="close-container">
                    <AiFillCloseSquare className="close-symbol" onClick={onHide} />
                </div>
            </Drawer>
        </ClickAwayListener>
    )
}
export default NavDrawer;