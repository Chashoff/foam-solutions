import React, { Component } from 'react';
import './nav.css';
import NavDrawer from '../drawers';
import { FaBars } from 'react-icons/fa';
import AOS from 'aos';
import 'aos/dist/aos.css';
AOS.init();



class NavBar extends Component {
    state = {
        isNav: false
    }
    handleNavClose = () => {
        this.setState({ isNav: !this.state.isNav });
    }
    render() {
        const logo = '/images/main_logo.svg';
        if(this.state.isNav === true) {
            return <NavDrawer isOpen={this.state.isNav} onHide={this.handleNavClose} />
        }
        return (
            <div className="nav-container">
                <div data-aos="fade-right" data-aos-duration="1000" className="c-container">
                    <FaBars className="hamburger-menu" onClick={this.handleNavClose} />
                    <div className="visit-container">
                        <p className="visit-txt">VISIT TODAY</p>
                        <img src="./images/right-arrow.png" alt="" />
                    </div>
                </div>
                <div data-aos="fade-left" data-aos-duration="1000" className="d-container">
                    <p className="d-item">2251 Performance Parkway Columbus, OH 43207</p>
                    <p className="d-item">1-614-586-1326</p>
                </div>
            </div>
        )
    }
}
export default NavBar;